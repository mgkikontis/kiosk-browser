package com.one.kioskbrowser;


import com.one.kioskbrowser.R;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;


//back button is override, phone button is override
//home and other button are not override because this app is an home replacement
//the exit button finish this application and prompt the user for select a new home


public class MainActivity extends Activity {


	private Handler screen_handler = new Handler();
	private Button exit_btn;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		//set fullscreen and no title
		//requestWindowFeature(Window.FEATURE_NO_TITLE);
		//getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,  WindowManager.LayoutParams.FLAG_FULLSCREEN);

		//set main view
		setContentView(R.layout.browser_view);
		//configure the main view        
		WebView mWebView = (WebView) findViewById(R.id.webview);
		//declare exit button
		exit_btn = (Button) findViewById(R.id.button1);


		mWebView.setOnTouchListener(new View.OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				//reset_idle_timer();
				return false;
			}
		});


		//exit button action listener
		exit_btn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();

			}
		});

		Bundle bundle = getIntent().getExtras();
		String url_string = "";
		if (bundle != null){
			if(bundle.getString("url") != null){
				url_string = bundle.getString("url");
			}
			if (bundle.getString("button")!=null){
				exit_btn.setText(bundle.getString("button"));
			}
			if(bundle.getString("title") != null){
				getActionBar().setTitle(bundle.getString("title"));
			}
			else{
				getActionBar().setTitle("Kiosk Browser");
			}
		}
		else{
			url_string = "http://www.google.co.uk";
		}

		getActionBar().setHomeButtonEnabled(true);


		mWebView.setWebViewClient(new MainWebViewClient());
		mWebView.getSettings().setJavaScriptEnabled(true);
		mWebView.getSettings().setUserAgentString("Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.13 (KHTML, like Gecko) Chrome/24.0.1284.0 Safari/537.13");
		mWebView.getSettings().setAllowFileAccess(true);
		mWebView.getSettings().setPluginsEnabled(true);
		mWebView.loadUrl(url_string);
		mWebView.getSettings();
		//screen_handler.postDelayed(time_out, 30000);
	}

	//private void reset_idle_timer() {
	//screen_handler.removeCallbacks(time_out);
	//screen_handler.postDelayed(time_out, 30000);

	//}

	//public Runnable time_out = new Runnable(){
	//public void run(){
	//finish();
	//Log.i("Handler Event","Finished Browsing");
	//screen_handler.postDelayed(this, 300000);
	//}
	//};


	//draw the menu - we use this menu to exit the app
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.mainmenu, menu);
		return true;
	}        

	//menu items
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		LayoutInflater layoutinflater = LayoutInflater.from(this);
		final View textEntryView;
		switch (item.getItemId()) {
		case R.id.item_exit:
			textEntryView = layoutinflater.inflate(R.layout.exitdialog, null);
			new AlertDialog.Builder(this)
			.setView(textEntryView)
			.setIcon(android.R.drawable.ic_dialog_alert)
			.setTitle("Exit")
			.setPositiveButton("OK", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					//if the password is correct we can exit!
					if(
							((EditText)textEntryView.findViewById(R.id.exitpassword)).getText().toString()
							.compareTo(getString(R.string.exitpassword)) == 0)
					{
						//prompt the user for the preferred home
						Intent selector = new Intent("android.intent.action.MAIN");
						selector.addCategory("android.intent.category.HOME");
						selector.setComponent(new ComponentName("android", "com.android.internal.app.ResolverActivity"));
						startActivity(selector);

						//finish this activity
						finish();
					}
				}
			})
			.setNegativeButton("CANCEL", null)
			.show(); 
			return true;
		case android.R.id.home:
			finish();
			return true;
		}
		return false;
	}

	//override some buttons
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		//Log.d("button", new Integer(keyCode).toString());

		if ((keyCode == KeyEvent.KEYCODE_BACK)) {
			return true;
		}
		else if ((keyCode == KeyEvent.KEYCODE_CALL)) {
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

}